﻿// See https://aka.ms/new-console-template for more information
global using System.Text;
using Meeting;
using System.Data;
using System.Security.Cryptography;
using System.Xml.Linq;

Console.WriteLine("Добро пожаловать в Ежедневник!\n\n" );

var meetBase = new Dictionary<DateTime, Meeting.Meeting>();

int automatState = 0;

while (true)
{
    switch (automatState)
    {
        case -1:
            ErrorMsg("Некорректный ввод, повторите попытку");
            automatState = 0;
            break;
        case 0:
            HelpDialog();
            automatState = GiveMeInstruction();
            break;
        case 1: 
            CreateMeeting();
            automatState = GiveMeInstruction();
            break;
        case 2:
            DeleteMeeting();
            automatState = GiveMeInstruction();
            break;
        case 3:
            RemakeMeeting();
            automatState = GiveMeInstruction();
            break;
        case 4:
            DateTime data = GetTime("просмотра");
            LookDaList(data);
            automatState = GiveMeInstruction();
            break;
    }
}

void CreateMeeting()
{
    string name;
    Meeting.Meeting newMeeting;

    do
    {
        Console.WriteLine("\nВведите название встречи и нажмите Enter\n");
        name = Console.ReadLine();
    }
    while (name.Length == 0);

    GetMeetingTime(out DateTime meetingTime, out DateTime endTime);

    TimeToAlarm(meetingTime, out DateTime alarmTime);

    if (alarmTime.Year == 1999)
    { newMeeting = new Meeting.Meeting(name, meetingTime, endTime); }
    else { newMeeting = new Meeting.Meeting(name, meetingTime, endTime, alarmTime); }
    
    SaveMeeting(newMeeting);
}

void DeleteMeeting()
{
    var date = GetTime("удаления");
    
    LookDaList(date);
  
    Console.WriteLine("\nВведите имя встречи, которую вы хотели бы удалить\n");

    string name = Console.ReadLine();

    DateTime removable = GetKeyMeetingByName(name);

    if(removable.Year != 1999) meetBase.Remove(removable);
    else { ErrorMsg("Не существует встречи с таким именем"); }
}

static void ErrorMsg(string errMsg)
{
    Console.Beep();
    Console.WriteLine(errMsg);
}

static void GetMeetingTime(out DateTime startTime, out DateTime endTime)
{
    startTime = new DateTime(1999, 01, 01, 0, 0, 0);
    endTime = new DateTime(1999, 01, 01, 0, 0, 1);

    bool error = false;

    while (!error)
    {
        startTime = GiveMeTime("начала");

        endTime = GiveMeTime("окончания");

        error = ((startTime <= endTime) && (startTime > DateTime.Now)) ? true : false;

        if (!error)
        {
            ErrorMsg("\nНельзя планировать встречи на прошедшее время." +
                        "\nВремя окончания встречи должно быть позже времени начала.");
        }
    }
}

DateTime GetKeyMeetingByName(string name)
{
    DateTime Key = new DateTime(1999, 1, 1, 0, 0, 1);
    foreach (var a in meetBase)
    {
        if (a.Value.Name == name)
        {
            return  Key = a.Key;
        }
    }
    return Key;
}

static DateTime GetTime(string mov)
{
    bool chekData = false;
    DateTime date = new DateTime();

    while (!chekData)
    {
        Console.WriteLine("\nВведите дату для " + mov + " запланированных встреч\n");

        string input = Console.ReadLine();

        if (DateTime.TryParse(input, out date))
        {
            chekData = true;
        }
        else 
        {
            ErrorMsg("\nНекорректный ввод, повторите попытку\n");
        }

    }
    return date;
}

static int GiveMeInstruction()
{
    int instruction = 0;

    Console.WriteLine("\nВведите требуемую инструкцию в строку и нажмите Enter\n");

    string input = Console.ReadLine(); 

    if (input.Length == 1)
    {
        char[] cmd = input.ToCharArray();

        switch (cmd[0])
        {
            case 'c': instruction = 1; break;
            case 'd': instruction = 2; break;
            case 'r': instruction = 3; break;
            case 'l': instruction = 4; break;
            case 'h': instruction = 0; break;
            default: instruction = -1; break;
        }

    }

    return instruction;
}

static DateTime GiveMeTime(string pos)
{
    bool chekData = false;
    DateTime date = new DateTime();

    while (!chekData)
    {
        Console.WriteLine("\nВведите дату и время " + pos+ " встречи в формате: ");
        Console.WriteLine("ДД/ММ/ГГГГ ЧЧ:ММ:СС");
        Console.WriteLine("и нажмите Enter.\n");

        string input = Console.ReadLine();

        if (DateTime.TryParse(input, out date))
        {
            chekData = true; 
        }
        else 
        {
            ErrorMsg("\nНекорректный ввод, повторите попытку\n");
        }

    }
    return date;
}

static DateTime GiveAlarmTime()
{
    bool chekData = false;
    DateTime date = new DateTime();

    while (!chekData)
    {
        Console.WriteLine("За какой промежуток времени Вас оповестить о встрече?(ЧЧ:ММ:СС)");

        string input = Console.ReadLine();

        if (DateTime.TryParse(input, out date))
        {
            chekData = true;
        }
        else
        {
            ErrorMsg("\nНекорректный ввод, повторите попытку\n");
        }

    }

    return date;
}

static void HelpDialog()
{
    Console.WriteLine("Перечень доступных команд:\n\nc - создание новой встречи;");
    Console.WriteLine("d - удаление существующей встречи;");
    Console.WriteLine("r - изменение данных о встрече; ");
    Console.WriteLine("l - вывод перечня существующих встреч;");
    Console.WriteLine("h - помощь");
}

void LookDaList(DateTime data)
{
    bool err = false;
    
    foreach (var a in meetBase)
    {
        if (a.Key.Date.Equals(data.Date))
        {
            WriteAboutMeeting(a.Value);
            err = true;
        }
    }
    if (!err) Console.WriteLine("\nУ вас нет запланированных встреч\n");
    else {
             if (automatState == 4)
             {
                  Console.WriteLine("Чтобы сохранить результаты в формате .txt нажмите P\n" +
                                "Чтобы продолжить нажмите N\n");
                  if (Console.ReadKey().Key == ConsoleKey.P)
                  {
                     PrintResult(data);
                  }
             }
         }
}


void PrintResult(DateTime data)
{
    StringBuilder name = new StringBuilder();
    name.Append(data.DayOfYear.ToString());
    name.Append(".txt");
    string path = Path.Combine(name.ToString());

    //if (File.Exists(path))
    //{ File.Delete(path); }

    FileStream metList = new FileStream(path, FileMode.Create);

    while (!File.Exists(path)) Console.WriteLine("Создаю");

    metList.Close();

    Console.WriteLine("File created");

    if (File.Exists(path))
    {
        using (StreamWriter ml = new StreamWriter(path))
        {
            foreach (var a in meetBase)
            {
                if (a.Key.Date.Equals(data.Date))
                {
                    ml.WriteLine("\nName: " + a.Value.Name +
                                  "\nStart: " + a.Value.StartTime +
                                 "\nEnd: " + a.Value.EndTime + "\n");
                }

                Console.WriteLine("Запись файла завершена");
            }
        }
    }
    else { Console.WriteLine("Что-то пошло не так"); }

}

void RemakeMeeting()
{
    var date = GetTime("изменения");
    bool err = false;
    LookDaList(date);
    
    Console.WriteLine("\nВведите имя встречи, которую вы хотели бы изменить\n");

    while (!err)
    {
        string namech = Console.ReadLine();

        DateTime changKey = GetKeyMeetingByName(namech);

        if (changKey.Year != 1999)
        {
            Console.WriteLine("Выберите изменяемый параметр:\n" +
                                "n - название встречи\n" +
                                "t - время проведения встречи\n" +
                                "a - настройка уведомлений");

            string chang = Console.ReadLine();

            switch (chang)
            {
                case "n":
                    Console.WriteLine("Введите новое имя");
                    string newName = Console.ReadLine();
                    meetBase[changKey].Name = newName;
                    WriteAboutMeeting(meetBase[changKey]);
                    err = true;
                    break;
                case "t"://ChekMeImTrue
                    GetMeetingTime(out DateTime newStart, out DateTime newEnd);
                    Meeting.Meeting newMeet = new Meeting.Meeting(meetBase[changKey].Name,
                                                                            newStart, newEnd);
                    bool may = ChekMeImNew(newMeet);
                    if (may)
                    {
                        //meetBase[changKey].StartTime = newStart;
                        //meetBase[changKey].EndTime = newEnd;
                        if (changKey != newMeet.StartTime)
                        {
                            //Meeting.Meeting newMeet = new Meeting.Meeting(meetBase[changKey].Name,
                            //                                                         newStart, newEnd);
                            meetBase.Remove(changKey);
                            meetBase.Add(newMeet.StartTime, newMeet);
                            WriteAboutMeeting(meetBase[newMeet.StartTime]);
                        }
                        else 
                        {
                            meetBase[changKey] = newMeet;
                            WriteAboutMeeting(meetBase[changKey]); 
                        }
                        err = true; 
                    }
                    else {err = true;  }
                    
                    break;
                case "a":
                    TimeToAlarm(meetBase[changKey].StartTime, out DateTime newAlarm);
                    meetBase[changKey].AlarmTime = newAlarm;
                    WriteAboutMeeting(meetBase[changKey]);
                    err = true;
                    break;
                default:
                    ErrorMsg("Неверная команда");
                    err = false;
                    break;
            }
        }
        else
        {
            ErrorMsg("Неверно введено имя");
            err = false;
        }
    }


}

void SaveMeeting(Meeting.Meeting meeting)
{
    bool may = false;

    if (meetBase.Count == 0)
    { meetBase.Add(meeting.StartTime, meeting); }
    else
    {
        may = ChekMeImNew(meeting);

        if (may) meetBase.Add(meeting.StartTime, meeting);

    }
}

bool ChekMeImNew(Meeting.Meeting meeting)
{
    foreach (var a in meetBase)
    {
        if ((a.Value.StartTime >= meeting.StartTime && a.Value.StartTime <= meeting.EndTime) ||
            (a.Value.EndTime >= meeting.StartTime && a.Value.EndTime <= meeting.EndTime) ||
            (a.Value.StartTime <= meeting.StartTime && a.Value.EndTime >= meeting.EndTime) ||
            (a.Value.StartTime >= meeting.StartTime && a.Value.EndTime <= meeting.EndTime))
        {
            Console.WriteLine("\nУ вас уже запланированы встречи на это время\n");
            return false;
        }
        else { return true; }
    }
    return false;
}

static void TimeToAlarm(DateTime start, out DateTime alarm)
{
    alarm = new DateTime(1999, 01, 01, 0, 0, 0);
    bool err = false;
    Console.WriteLine("\nЖелаете настроить уведомление о встрече? (Y/N)\n");
    ConsoleKeyInfo cmd = Console.ReadKey();
    while (!err)
    {
        if (cmd.Key == ConsoleKey.Y)
        { 
            DateTime predTime = GiveAlarmTime();
            if ( (start.Hour - predTime.Hour) > 0)
            {
                if ((start.Minute - predTime.Minute) >= 0)
                {
                    alarm = new DateTime(start.Year, start.Month, start.Day, start.Hour - predTime.Hour,
                                        start.Minute - predTime.Minute, 0);
                }
                else
                {
                    alarm = new DateTime(start.Year, start.Month, start.Day, start.Hour - predTime.Hour-1,
                                        60 - predTime.Minute, 0);
                }
            }
            else 
            {
                if ((start.Minute - predTime.Minute) >= 0)
                {
                    alarm = new DateTime(start.Year, start.Month, start.Day - 1,
                                                24 - (predTime.Hour - start.Hour),
                                                    start.Minute - predTime.Minute,0);
                }
                else
                {
                    alarm = new DateTime(start.Year, start.Month, start.Day - 1, 
                                                 24 - (predTime.Hour - start.Hour)-1,
                                                      60 - predTime.Minute, 0);
                }
            }
            err = true;
        }
        else if (cmd.Key == ConsoleKey.N)
        { 
            err = true;
        }
        else { 
                err = false;
                ErrorMsg("Некорректный ввод");
                cmd = Console.ReadKey();
             }
    }
}

void WriteAboutMeeting(Meeting.Meeting meeting)
{
    if (meeting.AlarmOf)
    {
        Console.WriteLine("\nName: " + meeting.Name +
                    "\nStart: " + meeting.StartTime +
                    "\nEnd: " + meeting.EndTime + "\n");
    }
    else
    {
        Console.WriteLine("\nName: " + meeting.Name +
                    "\nStart: " + meeting.StartTime +
                    "\nEnd: " + meeting.EndTime + 
                    "\nAlarm:" + meeting.AlarmTime);
    }
}


