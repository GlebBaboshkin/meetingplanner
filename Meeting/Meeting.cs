﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace Meeting
{
    internal class Meeting
    {
        protected string name;
        protected DateTime startTime;
        protected DateTime endTime;
        protected DateTime alarmTime;

        protected bool alarmOf = true;
        public Meeting(string _name, DateTime _startTime, DateTime _endTime)
        {
            name = _name;
            startTime = _startTime;
            endTime = _endTime;
        }

        public Meeting(string _name, DateTime _startTime, DateTime _endTime, DateTime _alarmTime)
        {
            name = _name;
            startTime = _startTime;
            endTime = _endTime;
            alarmTime = _alarmTime;
            AlarmOn();
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }

        public DateTime AlarmTime
        {
            get { return alarmTime; }
            set 
            { 
                alarmTime = value;
                if (alarmOf) AlarmOn();
            }
        }

        public bool AlarmOf {get {return alarmOf;} }

        protected void Alarm()
        {
            Console.Beep();
            Thread.Sleep(1000);
            Console.Beep();
            Thread.Sleep(1000);
            Console.Beep();
            Thread.Sleep(1000);
            Console.WriteLine("\n\n\nНапоминание о встрече\n\n\n" +
                                    "Имя:" + this.Name + "\n" +
                                    "Начало:" + this.StartTime + "\n");
        }

        protected void AlarmOn()
        {
            alarmOf = false;

            Thread alarm = new Thread(WaitAlarm);

            alarm.Start();
        }

        public DateTime EndTime
        {
            get { return endTime; }
            set { endTime = value; }
        }

        void WaitAlarm()
        {
            alarmOf = false;
            while (!alarmOf)
            {
                Thread.Sleep(10000);
                if (DateTime.Now >= alarmTime)
                {
                    Alarm();
                    alarmOf = true;
                }
            }
        }
    }
}
